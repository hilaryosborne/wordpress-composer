import $ from "jquery";
import { doAppSetup } from "./app/main";

import "./scss/main.scss";

$(document).ready(() => {
	doAppSetup();
});

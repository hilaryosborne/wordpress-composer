Project Setup
---
First Run
`composer install
`

Then run `npm install`

Then run `docker-composer build`

Then run `docker-composer up`

WebPack Config
---
In webpack.config.js change to the theme folder name
~~~
const theme = 'theme_folder_name';
~~~

WordPress Assets
---
All assets such as themes, plugins and uploads go in the public/content folder.

Include the following code within your functions.php to include the generated js/css

~~~~

function webpack_setup() {
	$j_manifest = json_decode(file_get_contents(dirname(__FILE__) . '/dist/manifest.json'),true);
	if (isset($j_manifest['main']['css'])) {
        wp_enqueue_style('webpackcss', get_template_directory_uri() . '/dist/'.$j_manifest['main']['css']);
    }
    if (isset($j_manifest['main']['js'])) {
        wp_enqueue_script('webpackjs', get_template_directory_uri() . '/dist/'.$j_manifest['main']['js'], true);
    }
}

add_action('init', 'webpack_setup');
~~~~

Running WebPack
---

To build the assets in dev mode
~~~
yarn build:dev
~~~

To build the assets in distribution mode
~~~
yarn build:dev
~~~

The built assets will be put into a dist folder within your theme folder.
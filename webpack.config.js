const path = require('path');
const production = process.argv.indexOf('-p') != -1;
const webpack = require('webpack');
const fileloader = require('file-loader');
const postcss = require('postcss-loader');
const urlloader = require('url-loader');

const AssetsPlugin = require('assets-webpack-plugin');
const CleanPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const theme = 'THEME FOLDER';

const extractSass = new ExtractTextPlugin({
  filename: "css/[name].[hash].css",
  disable: production
});

const assets = new AssetsPlugin({
  filename: 'manifest.json',
  path: path.resolve(`./public/content/themes/${theme}/dist/`),
  fullPath: false
});

const clean = new CleanPlugin(`./public/content/themes/${theme}/dist/`);

const uglify =  new webpack.optimize.UglifyJsPlugin({
  beautify: false,
  mangle: {
    screw_ie8: true,
    keep_fnames: true
  },
  compress: {
    screw_ie8: true
  },
  comments: false
});

const aggressive = new webpack.optimize.AggressiveMergingPlugin();

module.exports = {
	context: path.resolve(`${__dirname}/src`),
	devtool: !production ? "inline-sourcemap" : null,
	entry: ["babel-polyfill","./index.js"],
	node: {
		fs: "empty"
	},
  resolve: {
    extensions: ['.js', '.jsx']
  },
	module: {
		loaders: [
			{ test: /bootstrap\/js\//, loader: 'imports?jQuery=jquery' },
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        loader: 'babel-loader'
      },
      {
        test: /\.jsx$/,
        exclude: /(node_modules)/,
        loader: 'babel-loader',
        query: {
          presets:['react']
        }
      },
			{
				test: /\.scss$/,
				exclude: /(node_modules)/,
        use: extractSass.extract({
          use: [
            {loader:"css-loader", options:{sourceMap: true}},
            'resolve-url-loader',
            {
              loader: 'postcss-loader',
              options: { sourceMap: true }
            },
            {
              loader: 'sass-loader',
              options: { sourceMap: true }
            }
          ],
          fallback: "style-loader"
        })
			},
			{
        test: /\.(eot|woff|woff2?|ttf)$/,
        exclude: /node_modules/,
        use: ['file-loader?name=fonts/[name].[ext]']
			},
      {
        test: /\.(png|gif|jpe?g|svg)$/i,
        use: ['url-loader?limit=10000&name=images/[name].[ext]&context=src/app']
      },
		]
	},
	output: {
		path: path.resolve(`./public/content/themes/${theme}/dist/`),
		filename: !production ? "site.[chunkhash].js" : "site.js",
		publicPath: '/content/themes/'+theme+'/dist/',
	},
	plugins: !production ? [ clean, assets, extractSass ] : [ clean, extractSass, assets, uglify, aggressive  ]
};
